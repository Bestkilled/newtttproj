
import java.util.Scanner;

public class OxGames {

    private Scanner kb = new Scanner(System.in);
    private Players playX;
    private Players playO;
    private Players turn;
    private Table table;
    private int row, col;

    public OxGames() {
        playX = new Players("X");
        playO = new Players("O");
        table = new Table(playX, playO);
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            table.switchPlayer();
            if (table.isFinish()) {
                if (table.winner() == null) {
                    this.showTable();
                    System.out.println("Draw!!!!!");
                    break;
                } else {
                    this.showTable();
                    System.out.println(table.winner().getName() + " win . . . .");
                    break;
                }

            }
        }

    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.ShowTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Cal: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if ((row >= 0 && row <= 2 && col >= 0 && col <= 2)) {
                if (table.setRowCol(row, col)) {
                    break;
                } else {
                    System.out.println("Error: Pls select another position!!!");
                }
            } else {
                System.out.println("Error: Pls select another position!!!");
            }
        }
    }

    public void showTurn() {
        System.out.println(table.getCurplayer().getName() + " turn");
    }
}
