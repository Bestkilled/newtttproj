/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author asus
 */

public class Unittest {
    Table table;
    Players o,x;
    
    @Test
    public void setDefax() {
        x = new Players("X");
        o = new Players("O");
        table = new Table(x,o);
    }
    public void setDefao() {
        x = new Players("X");
        o = new Players("O");
        table = new Table(o,x);
    }
//    Row
    public void testRowx1() {
        setDefax();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.winner());  
    }
    public void testRowx2() {
        setDefax();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.winner());  
    }
    public void testRowx3() {
        setDefax();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.winner());  
    }
//    col
    public void testColx1() {
        setDefax();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.winner());  
    }
    public void testColx2() {
        setDefax();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.winner());  
    }
    public void testColx3() {
        setDefax();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.winner());  
    }
    public void testX1() {
        setDefax();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.winner());  
    }
    public void testX2() {
        setDefax();
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x,table.winner());  
    }
    
    //    Row
    public void testRowo1() {
        setDefao();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.winner());  
    }
    public void testRowo2() {
        setDefao();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.winner());  
    }
    public void testRowo3() {
        setDefao();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.winner());  
    }
//    col
    public void testColo1() {
        setDefao();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.winner());  
    }
    public void testColo2() {
        setDefao();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.winner());  
    }
    public void testColo3() {
        setDefao();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.winner());  
    }
    public void testO1() {
        setDefao();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.winner());  
    }
    public void testO2() {
        setDefao();
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o,table.winner());  
    }
//    one position
    public void testOnex1() {
        setDefax();
        table.setRowCol(0, 0);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    public void testOnex2() {
        setDefax();
        table.setRowCol(1, 1);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    //    two position 
    public void testTwox1() {
        setDefax();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    public void testTwox2() {
        setDefax();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    public void testTwox3() {
        setDefax();
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
//    one position
    public void testOneo1() {
        setDefao();
        table.setRowCol(0, 0);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    public void testOnexo() {
        setDefao();
        table.setRowCol(1, 1);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    //    two position 
    public void testTwoo1() {
        setDefao();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    public void testTwoo2() {
        setDefao();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    public void testTwoo3() {
        setDefao();
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false, table.isFinish());  
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
